import sys
import csv
#import numpy
import package.send_to
import package.calc
import package.get_sensor
import datetime
import socket

PORT = 3025
BUFFER_SIZE = 4096

try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('192.168.100.100', PORT))
        s.listen()
        (connection, client) = s.accept()

        with open('altitude13.csv', 'w') as csv_file:
            fieldnames = ['Time', 'Dist_head', 'Strength_head', 'Mode_head', 'Dist_tail', 'Strength_tail', 'Mode_tail', 'Altitude', 'Pitch']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            while(True):
                try:
                    from rplidar import RPLidar
                    lidar = RPLidar('/dev/ttyUSB0')	
                    for i, scan in enumerate(lidar.iter_scans()):
                        for j in scan:
                            angle, length = j[1],j[2]
                            int_angle = int(angle)
                            meter_length = length / 1000
                            meter_length2 = round(meter_length,2)
    
                            strid_data = str(i)
                            strangle = str(int_angle)
                            strlength = str(meter_length2)
    
                            Dist_head, Strength_head, Mode_head, Dist_tail, Strength_tail, Mode_tail = package.get_sensor.get_lidar_st()
    
                            now_time = datetime.datetime.now()
                            dt_now = str(now_time)[5:-3]	
                            altitude, pitch = package.calc.altitude_calc(Dist_head, Dist_tail)
                            altitude_f = round(altitude, 3)
                            pitch_f = round(pitch, 1)
                            intaltitude = int(altitude)
                            straltitude = str(intaltitude)
                            
                            senddata = strid_data + ',' + strangle + ',' + strlength + ',' + straltitude + ','
                            connection.send(senddata.encode())
    
                            package.send_to.send_to_fcu(altitude)
                            writer.writerow({'Time':dt_now, 'Dist_head':Dist_head, 'Strength_head':Strength_head, 'Mode_head':Mode_head, 'Dist_tail':Dist_tail, 'Strength_tail':Strength_tail, 'Mode_tail':Mode_tail, 'Altitude':altitude_f, 'Pitch':pitch_f})
                except Exception as ex1:
                    print(ex1)
                    print(ex1.__class__.__name__)
                    if ex1.__class__.__name__ == "ConnectionResetError" or ex1.__class__.__name__ == "BrokenPipeError":
                        sys.exit(0)
except KeyboardInterrupt: 
	sys.exit(0)
