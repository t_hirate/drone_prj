## proof of concept

from smbus2 import SMBus, i2c_msg
#import time

address = 0x10
Dist_head_pre = 0

# write Reg_H, Reg_L and Data Length to the sensor, without (!) a STOP
# request read, specifying the sensor and 7 bytes

write = i2c_msg.write(address, [1, 2, 7])
read = i2c_msg.read(address, 7)

def get_lidar_st():
    global Dist_head_pre
    
    with SMBus(0) as bus0: #bus number 0
        #start = time.time()
        bus0.i2c_rdwr(write, read)
        data = list(read)
        #TrigFlag = data[0]
        Dist_head = ((data[3] << 8 | data[2]))
        Dist_head = Dist_head + 3
        Strength_head = ((data[5] << 8 | data[4]))
        Mode_head = (data[6])
		
    with SMBus(1) as bus1: #bus number 1
        #start = time.time()
        bus1.i2c_rdwr(write, read)
        data = list(read)
        #TrigFlag = data[0]
        Dist_tail = ((data[3] << 8 | data[2]))
        Dist_tail = Dist_tail + 4
        Strength_tail = ((data[5] << 8 | data[4]))
        Mode_tail = (data[6])
    
    
    if (Mode_head != 0):
        Dist_head = Dist_head_pre
    
    Dist_head_pre = Dist_head

    return Dist_head, Strength_head, Mode_head, Dist_tail, Strength_tail, Mode_tail

