# =============================================================================
# import serial
# import numpy
# =============================================================================
import math

def altitude_calc(lidar_head, lidar_tail):
    #global altitude, pitch
    altitude = 0.0
    pitch = 0.0

    body = 20.5

    center = (lidar_head + lidar_tail) / 2.0
    #print(center)

    tan_pitch = (lidar_head - lidar_tail) / body
    pitch = math.degrees(math.atan(tan_pitch))
    #print(math.degrees(math.atan(tan_pitch)))

    cos_pitch = 1.0 / math.sqrt(1.0 + (tan_pitch**2))
    #print(math.degrees(math.acos(cos_pitch)))

    altitude = center * cos_pitch
    #print(altitude)
    return altitude, pitch


if __name__ == '__main__':
    altitude_calc()
    