# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 16:05:16 2022

@author: scsk-kato
"""

import serial
import numpy
import package.calc
import package.get_sensor


ser = serial.Serial(
	port = '/dev/ttyTHS1',
	baudrate = 3000000,
	bytesize = serial.EIGHTBITS,
	parity = serial.PARITY_NONE,
	stopbits = serial.STOPBITS_ONE,
        timeout = None,
)

def send_to_fcu(data):
    na_int = numpy.array([data], numpy.uint16)
    ser.write(na_int)

def send_altitude():
    while True:
        altitude = package.calc.altitude_calc(package.get_sensor.get_lidar_st()[0], package.get_sensor.get_lidar_st()[3])[0]
        na_int = numpy.array([altitude], numpy.uint16)
        ser.write(na_int)
        #print(na_int)

