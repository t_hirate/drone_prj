import sys
import csv
#import numpy
import package.send_to
import package.calc
import package.get_sensor
import datetime
import socket

PORT = 3004
BUFFER_SIZE = 4096

try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind(('192.168.100.100', PORT))
        s.listen()
        (connection, client) = s.accept()

        with open('altitude13.csv', 'w') as csv_file:
            fieldnames = ['Time', 'Dist_head', 'Strength_head', 'Mode_head', 'Dist_tail', 'Strength_tail', 'Mode_tail', 'Altitude', 'Pitch']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            while(True):
                Dist_head, Strength_head, Mode_head, Dist_tail, Strength_tail, Mode_tail = package.get_sensor.get_lidar_st()

                now_time = datetime.datetime.now()
                dt_now = str(now_time)[5:-3]	
                altitude, pitch = package.calc.altitude_calc(Dist_head, Dist_tail)
                altitude_f = round(altitude, 3)
                pitch_f = round(pitch, 1)
                intaltitude = int(altitude)
                straltitude = str(intaltitude)
                straltitude = straltitude + ','
                connection.send(straltitude.encode())		

                package.send_to.send_to_fcu(altitude)
                writer.writerow({'Time':dt_now, 'Dist_head':Dist_head, 'Strength_head':Strength_head, 'Mode_head':Mode_head, 'Dist_tail':Dist_tail, 'Strength_tail':Strength_tail, 'Mode_tail':Mode_tail, 'Altitude':altitude_f, 'Pitch':pitch_f})

except KeyboardInterrupt: 
	sys.exit(0)
